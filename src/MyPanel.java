package de.vikibot.main;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import javax.swing.JPanel;

import javax.script.*;

class MyPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	
	private String function = "x";
	Dimension dimension = new Dimension(200,200);
	Rectangle visualPaneValues = new Rectangle(-100,100,dimension.width,dimension.height);
	double yRelationInputToPane = Double.valueOf(visualPaneValues.height) / dimension.height;

	ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("nashorn");
    
	public Dimension getPreferredSize() {
        return dimension;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        // Relations x,y input to PaintPanel
        Double yVPToInputValuesRelation = Double.valueOf(visualPaneValues.height) 
        		/ Double.valueOf(dimension.height);
        Double xVPToInputValuesRelation = Double.valueOf(visualPaneValues.width) 
        		/ Double.valueOf(dimension.width);
        
        // Coordinate system lines
        int xCoordinateLineValue = Double.valueOf(Double.valueOf(visualPaneValues.y) 
        		/ yVPToInputValuesRelation).intValue();
        g.drawLine(0,xCoordinateLineValue,dimension.width,xCoordinateLineValue);
        int yCoordinateLineValue = Double.valueOf(Double.valueOf(visualPaneValues.x)
        			* -1.0
        		/ xVPToInputValuesRelation).intValue();
        g.drawLine(yCoordinateLineValue, 0, yCoordinateLineValue, dimension.height);
        
        Double tempDouble = 0.0;
        // calc current y value of graph
        try {
        	// x-Step for f(x) calculation
    		Float xStep = Float.valueOf("" + xVPToInputValuesRelation);
    		Float initialX = Float.valueOf(visualPaneValues.x);
        	int i = 0;
        	while(i < dimension.width) {
        		// y as String after script evaluation
        		String yAsString = engine.eval("" + 
        			function.replace("x", Float.valueOf(
        					initialX + i * xStep
        					).toString()
        				) 
        			+ ";").toString();
        		// y for painting
        		Double dY = (dimension.height - 
        				Double.valueOf(yAsString)
        				/ yRelationInputToPane
        				- (dimension.height - xCoordinateLineValue));
        		if(i == 0) {
        			tempDouble = dY;
        		}
        		// draw the p(x,y) 
        		if((initialX + i * xStep) < 0) {
        			g.drawLine(i,dY.intValue(),i,tempDouble.intValue());
        		}else {
        			g.drawLine(i,tempDouble.intValue(),i,dY.intValue());
        		}
        		
    			//g.drawLine(i-1,dY.intValue(),i,dY.intValue());
    			tempDouble = dY;
    			i++;
        	}
        	
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }  
    
    public void setDimension(Dimension dim) {
    	dimension.width = dim.width;
    	dimension.height = dim.height / 2;
    }
    
    public void setFunction(String function) {
    	this.function = function;
    }
    
    // transfer input values into visualPane Rectangle
    public void setVisualPaneValues(Rectangle rect) {
    	visualPaneValues.x = rect.x;
    	visualPaneValues.y = rect.y;
    	visualPaneValues.width = rect.width - rect.x;
    	visualPaneValues.height = rect.y - rect.height;
    	
    	setYRelationInputToPane();
    }
    
    public Rectangle getVisualPaneValues() {
    	return visualPaneValues;
    }
    
    public Dimension getDimension() {
    	return dimension;
    }
    
    public void setYRelationInputToPane() {
    	yRelationInputToPane = Double.valueOf(visualPaneValues.height) / dimension.height;
    }
}
