package de.vikibot.main;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

class Gui {
	
    public static void main(String args[]){
    	JFrame frame = new JFrame("Graph me!");
    	MyPanel mp = new MyPanel();
    	frame.addComponentListener(new ComponentAdapter() {
    		public void componentResized(ComponentEvent evt) {
                Component c = (Component)evt.getSource();
                System.out.println(c.getSize());
                mp.setDimension(c.getSize());
            }
    	});
    	
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setSize(300, 300);
    	JPanel mainPanel = new JPanel();
    	mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    	
    	// panel to draw on
    	
    	mainPanel.add(mp);
    	
    	JPanel panel = new JPanel();
    	panel.setLayout(new GridLayout(0,2));
    	
    	// input graph boundaries
    	panel.add(new JLabel("Left"));
    	JTextField jtfLeft = new JTextField("-100");
    	panel.add(jtfLeft);
    	panel.add(new JLabel("Top"));
    	JTextField jtfTop = new JTextField("100");
    	panel.add(jtfTop);
    	panel.add(new JLabel("Right"));
    	JTextField jtfRight = new JTextField("100");
    	panel.add(jtfRight);
    	panel.add(new JLabel("Bottom"));
    	JTextField jtfBottom = new JTextField("-100");
    	panel.add(jtfBottom);
    	
    	// frame onresize
    	frame.addComponentListener(new ComponentAdapter() {
    		public void componentResized(ComponentEvent evt) {
                Component c = (Component)evt.getSource();
                mp.setDimension(c.getSize());
            	mp.setVisualPaneValues(new Rectangle(
        				Integer.valueOf(jtfLeft.getText()),
        				Integer.valueOf(jtfTop.getText()),
        				Integer.valueOf(jtfRight.getText()),
        				Integer.valueOf(jtfBottom.getText())));
            }
    	});
    	
    	// function input
    	JTextField function = new JTextField("x");
    	mp.setFunction(function.getText());
    	panel.add(function);
    	
    	JButton drawButton = new JButton("Draw");
    	drawButton.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			mp.setFunction(function.getText());
    			mp.setVisualPaneValues(new Rectangle(
    				Integer.valueOf(jtfLeft.getText()),
    				Integer.valueOf(jtfTop.getText()),
    				Integer.valueOf(jtfRight.getText()),
    				Integer.valueOf(jtfBottom.getText())));
    			mp.repaint();
    		}
    	});
    	panel.add(drawButton);
    	
    	mainPanel.add(panel);
    	frame.add(mainPanel);
    	
    	// mouse interactions
    	Point pMouse = new Point();
    	mp.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				pMouse.setLocation(e.getPoint());
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("start:" + pMouse + 
						"; end:" + e.getPoint() + "; dimension:" + 
						mp.getWidth() + ";" + mp.getHeight());
				
				// positive moveX to the right; negative to the left
				double moveX = 1.0 * (pMouse.getX() - e.getX()) / mp.getWidth();
				mp.setFunction(function.getText());
				Rectangle rect = mp.getVisualPaneValues();
				
				// positive moveY to the top; negative to the bottom
				double moveY = 1.0 * (e.getY() - pMouse.getY()) / mp.getHeight();
				
				// Amount of pixels to move
				Double intMoveX = rect.width * moveX;
				Double intMoveY = rect.height * moveY;
				System.out.println(intMoveX);
				System.out.println(intMoveY);
				System.out.println(rect);
				jtfLeft.setText("" + (rect.x + intMoveX.intValue()));
				jtfTop.setText("" + (rect.y + intMoveY.intValue()));
				jtfRight.setText("" + (rect.x +  + rect.width + intMoveX.intValue()));
				jtfBottom.setText("" + (rect.y + - rect.height + intMoveY.intValue()));
    			mp.setVisualPaneValues(new Rectangle(
    				Integer.valueOf(jtfLeft.getText()),
    				Integer.valueOf(jtfTop.getText()),
    				Integer.valueOf(jtfRight.getText()),
    				Integer.valueOf(jtfBottom.getText())));
    			mp.repaint();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
    		
    	});
    	
    	frame.pack();
    	frame.setVisible(true);
    }
}
